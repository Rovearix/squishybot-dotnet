﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using squishybot_dotnet.Utilities;

namespace squishybot_dotnet.Services
{
    public class CommandHandlingService
    {
        // Command Prefix
        public const string Prefix = "~!";

        // Service Variables
        private readonly CommandService _commands;
        private readonly DiscordSocketClient _discord;
        private readonly IServiceProvider _services;

        // Constructor
        public CommandHandlingService(IServiceProvider services)
        {
            // Hooks CommandExecuted to handle post-command-execution logic
            _commands = services.GetRequiredService<CommandService>();
            _commands.CommandExecuted += CommandExecutedAsync;

            // Hooks MessageReceived to process each message that qualifies as a command
            _discord = services.GetRequiredService<DiscordSocketClient>();
            _discord.MessageReceived += MessageReceivedAsync;

            // Sets up services
            _services = services;

            // Sets up message scheduling
            new TimeyWimey(_discord).ScheduleUpdate();
        }

        // Registers modules that are public and inherit ModuleBase<T>
        public async Task InitializeAsync()
        {
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        // Listens for and parses new sent messages
        public async Task MessageReceivedAsync(SocketMessage rawMessage)
        {
            // Disregards potential bots' commands
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            // Checks for commands that need the listener
            if (UpdateUtility.Activated && UpdateUtility.Executor.Equals(rawMessage.Author))
            {
                UpdateUtility.UpdateDialog(rawMessage);
                return;
            }

            // This value holds the offset where the prefix ends
            var argPos = 1;

            // Performs prefix or mention check.
            if (!message.HasStringPrefix(Prefix, ref argPos) &&
                !message.HasMentionPrefix(_discord.CurrentUser, ref argPos)) return;

            // Uses the command service to perform execution with a command if one is matched
            var context = new SocketCommandContext(_discord, message);
            await _commands.ExecuteAsync(context, argPos, _services);
        }

        // Executes post command execution logic
        public async Task CommandExecutedAsync(Optional<CommandInfo> command, ICommandContext context, IResult result)
        {
            // Notifies the user if the parsed command was command not found
            if (!command.IsSpecified)
            {
                await context.Channel.SendMessageAsync("Sorry, that command was not found! Do "
                                                       + Prefix + "help for information.");
                return;
            }

            // Logic to run if the command was successful.
            if (result.IsSuccess)
                return;

            // Notifies the user if something that happened while running the command
            await context.Channel.SendMessageAsync($"Error: {result}");
        }
    }
}