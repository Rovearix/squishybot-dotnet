using System;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using squishybot_dotnet.Services;
using squishybot_dotnet.Utilities;

namespace squishybot_dotnet.Modules
{
    // Class where all the commands are stored
    public class PublicModule : ModuleBase<SocketCommandContext>
    {
        // Warns the user that they need to give a message
        [Command("announce")]
        [RequireContext(ContextType.Guild, ErrorMessage =
            "Sorry, this command must be ran from within a server, not a DM!")]
        public async Task AnnounceAsync()
        {
            if (Context.Guild.GetUser(Context.Message.Author.Id).Roles.Any(role => role.Name == "Executives"))
                await Context.Channel.SendMessageAsync("You need to give me something to say!");
            else
                await Context.Channel.SendMessageAsync(Config.RejectMessage);
        }

        // Sends a message from an executive in the voice of Mr.Squishy to the #fun channel
        [Command("announce")]
        [RequireContext(ContextType.Guild, ErrorMessage =
            "Sorry, this command must be ran from within a server, not a DM!")]
        public async Task AnnounceAsync([Remainder] string text)
        {
            if (Context.Guild.GetUser(Context.Message.Author.Id).Roles.Any(role => role.Name == "Executives"))
                await Context.Guild.GetTextChannel(Convert.ToUInt64(Config.PostChannel)).SendMessageAsync(text);
            else
                await Context.Channel.SendMessageAsync(Config.RejectMessage);
        }

        // Shows help command information
        [Command("help")]
        public async Task HelpAsync()
        {
            var message = "The whole purpose of me is to send our schools schedule every day at 7 AM EST";
            message += "\nFor information on a particular command, do `" + CommandHandlingService.Prefix +
                       "help` + `commandname`";
            message += "\n\nI have the following commands: `help`, `ping`, `order`, `times`,  `update`, and `announce`";
            await Context.Channel.SendMessageAsync(Context.Message.Author.Mention + ", " + message);
        }

        // Gives information about the various commands that can be run
        [Command("help")]
        public async Task HelpAsync([Remainder] string text)
        {
            // Sends the correcponding help message
            string message;
            switch (text.Split(" ")[0].ToLower())
            {
                case "announce":
                    message = "The `announce` command is used by executives to send a message to the server.";
                    break;
                case "help":
                    message =
                        "This command is too hard to explain how to use; try watching a few episodes of Rick and Morty.";
                    break;
                case "order":
                    message = "The `order` command posts the schedule early.";
                    break;
                case "ping":
                    message = "The `ping` command is for testing the the bot's command system";
                    break;
                case "times":
                    message = "The `times` command posts more in-depth information about today's schedule";
                    break;
                case "update":
                    message = "The `update` command is used to add unplanned events to the database.";
                    break;
                case "none":
                    message = "The whole purpose of me is to send our schools schedule every day at 7 AM EST";
                    message += "\nFor information on a particular command, do `" + CommandHandlingService.Prefix +
                               "help` + `commandname`";
                    message +=
                        "\n\nI have the following commands: `help`, `ping`, `order`, `times`,  `update`, and `announce`";
                    break;
                default:
                    message = "Sorry that command was not found! Do " + CommandHandlingService.Prefix +
                              "help for information.";
                    break;
            }

            await Context.Channel.SendMessageAsync(Context.Message.Author.Mention + ", " + message);
        }

        // Command that prints out the current order
        [Command("order")]
        public async Task OrderAsync([Remainder] string ignore = null)
        {
            await Context.Channel.SendMessageAsync(ScheduleGenerator.GenerateOrder());
        }

        // Test command that sends a message that is later edited
        [Command("ping")]
        public async Task PingAsync([Remainder] string ignore = null)
        {
            var message = await Context.Channel.SendMessageAsync("pong!");
            await message.ModifyAsync(msg => msg.Content = "Pranked");
        }

        // Command that prints out the current order's times
        [Command("times")]
        public async Task TimesAsync([Remainder] string ignore = null)
        {
            await Context.Channel.SendMessageAsync(ScheduleGenerator.GenerateTimes());
        }

        // Command that updated the event database
        [Command("update")]
        public async Task UpdateAsync([Remainder] string ignore = null)
        {
            // Checks the user's permissions
            if (Context.Guild.GetUser(Context.Message.Author.Id).Roles.Any(role => role.Name == "Executives"))
            {
                // Busy message if multiple people try to use the command
                if (!UpdateUtility.Activated)
                {
                    // Generates the initial info
                    var dialog = Context.Message.Author.Mention + ", welcome to the update dialog!\n";
                    dialog += "To cancel at any time, type `quit`\n";
                    dialog += "Type the date of the override `MM/dd/yyyy`";

                    // Switches on a listener
                    UpdateUtility.Activated = true;

                    // Saves current user
                    UpdateUtility.Executor = Context.Message.Author;

                    // Sends the message with instructions
                    await Context.Channel.SendMessageAsync(dialog);

                }
                else
                    await Context.Channel.SendMessageAsync(
                        "Sorry, another user is using this command. Check back later!");
            }
            else
                await Context.Channel.SendMessageAsync(Config.RejectMessage);
        }
    }
}