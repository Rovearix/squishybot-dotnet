﻿using System;

namespace squishybot_dotnet.Utilities
{
    public static class ScheduleGenerator
    {
        // Generates the message for the order command
        public static string GenerateOrder()
        {
            // Checks for school cancellation
            if (DateData.Check(GetDate()) == "No School" || DateData.Check(GetDate()) == "Cancel School")
            {
                // Checks for Tomorrow
                if ((DateTime.Now.Hour >= 15 || (DateTime.Now.Hour == 14 && DateTime.Now.Minute > 32)) ||
                    DateTime.Now.ToString("ddd").Substring(0, 1) == "S")
                    return "There is no school tomorrow.";
                return "There is no school today.";
            }
            return GenerateSchedule();
        }

        // Generates the message for the times command
        public static string GenerateTimes()
        {
            // Checks for weekend
            if (DateData.Check(GetDate()) == "No School" || DateData.Check(GetDate()) == "Cancel School")
                return "There is no school for today.";

            // String to hold the times
            var times = "```";

            // Checks for which schedule to post
            if (DateTime.Now.Hour >= 15 || (DateTime.Now.Hour == 14 && DateTime.Now.Minute > 32))
                times += "Tomorrow";
            else
                times += "Today";

            // Adds the date information
            times += " is a " + GetLetterDay() + " day and will be following the " + DateData.Check(GetDate()) + " schedule:";

            // Gets today's current times
            var todaysTimes = DateData.GetTimes();

            // Checks for Advisory or PLC
            if (DateData.Check(GetDate()) == "Advisory" || DateData.Check(GetDate()) == "PLC")
                times += "\n" + DateData.Check(GetDate()) + ": " + todaysTimes[9];


            // Adds the first 5 classes to the message
            for (var i = 0; i < 6; i++)
            {
                // Adds the current class start and end time to the message
                times += "\nClass " + GetNumberOrder().Replace(" ", "").ToCharArray()[i] + ": " + todaysTimes[i];

                // Checks for lunch occurrence
                if (i == 4 && todaysTimes.Length > 7)
                    for (var w = 1; w <= 3; w++)
                        times += "\n Wave " + w + ": " + todaysTimes[5 + w];
            }

            // Sends the text with the code block ending identifier
            return times + "```";
        }

        // Generates the format with the schedule data
        private static string GenerateSchedule()
        {
            // Variables to hold the schedule and date info
            string schedule = "", day = DateTime.Now.ToString("ddd");

            // Prints out the next day's schedule after school ends
            if ((DateTime.Now.Hour >= 15 || (DateTime.Now.Hour == 14 && DateTime.Now.Minute > 32)) ||
                day.Substring(0, 1) == "S")
            {
                schedule += "Tomorrow's schedule is as following:\n";
                if (day.Substring(0, 1) == "F" || day.Substring(1, 2) == "a")
                    return "There is no school tomorrow.";
            }

            // Adds the correct information
            schedule += "```";
            schedule += "Day:   " + GetLetterDay() + "                          Date: " + GetDate() + "\n";
            schedule += "Order: " + GetNumberOrder() + "                Type: " + DateData.Check(GetDate());
            schedule += "```";

            // Returns generated schedule
            return schedule;
        }

        // Gets the letter day from a file
        public static char GetLetterDay()
        {
            // Checks through each line
            var letter = DatabaseManager.ReadData("Today", "Letter")[0].ToCharArray()[0];

            // Prints out the next day's schedule after school ends
            if ((DateTime.Now.Hour >= 15 || (DateTime.Now.Hour == 14 && DateTime.Now.Minute > 32) ||
                 (DateTime.Now.Hour < 7)) || DateTime.Now.ToString("ddd").Substring(0, 1) == "S")
            {
                letter++;
                if (letter == 'I')
                    letter = 'A';
            }

            return letter;
        }

        // Gets the current date in a correct format
        public static string GetDate()
        {
            // Check for next day
            var postDate = DateTime.Now;
            if ((postDate.Hour >= 15 || (postDate.Hour == 14 && postDate.Minute > 32) ||
                 postDate.ToString("ddd").Substring(0, 1) == "S"))
                postDate = postDate.AddDays(1);

            // Returns the date in a simple format
            return postDate.ToString("MM/dd/yyyy");
        }

        // Gets the order of the current school day
        public static string GetNumberOrder()
        {
            var letter = GetLetterDay();
            if (letter == 'A')
                return "1 2 3 4 5 6";
            if (letter == 'B')
                return "7 8 1 2 3 4";
            if (letter == 'C')
                return "5 6 7 8 1 2";
            if (letter == 'D')
                return "3 4 5 6 7 8";
            if (letter == 'E')
                return "6 5 4 3 2 1";
            if (letter == 'F')
                return "4 3 2 1 8 7";
            return letter == 'G' ? "2 1 8 7 6 5" : "8 7 6 5 4 3";
        }
    }
}