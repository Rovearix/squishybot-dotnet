﻿namespace squishybot_dotnet.Utilities
{
    public static class Config
    {
        // String to hold the bot's token
        public static readonly string DiscordToken = DatabaseManager.ReadData("Config", "Token")[0];

        // Channel ID to auto post the schedule
        public static readonly string PostChannel = DatabaseManager.ReadData("Config", "Channel")[0];

        // Message that is sent when user is lacking perms
        public const string RejectMessage = "Sorry, it looks like your account is not authorized!";
    }
}