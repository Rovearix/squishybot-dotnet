﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace squishybot_dotnet.Utilities
{
    public class TimeyWimey
    {
        // Discord socket connection
        private readonly DiscordSocketClient _discord;

        // Construstor
        public TimeyWimey(DiscordSocketClient client)
        {
            _discord = client;
        }

        // Schedules the next calander update at the next 7 AM
        public void ScheduleUpdate()
        {
            // Date data for now and the future event
            var nextTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0);

            // Checks if the event should be posted the next day or the previous one
            if (nextTime < DateTime.Now)
                nextTime = nextTime.AddDays(1);
            var timeSpan = nextTime - DateTime.Now;

            // Schedules the post message event
            Task.Delay(timeSpan).ContinueWith((x) => UpdateCalender());
        }

        //Method that runs when the time occurs
        private async void UpdateCalender()
        {
            //Checks for planned no School
            if (DateData.Check(ScheduleGenerator.GetDate()) != "No School" &&
                DateTime.Now.ToString("ddd").Substring(0, 1) != "S")
            {
                //Increments the day
                IncrementDay();

                //Checks if school has been canceled and posts corresponding announcement
                var message = ScheduleGenerator.GenerateTimes();
                if (DateData.Check(ScheduleGenerator.GetDate()) == "Cancel School")
                    message = "```School is canceled today due to unforeseen circumstances. \n" +
                              "Have fun on your day off!```";
                await ((ITextChannel) _discord.GetChannel(Convert.ToUInt64(Config.PostChannel)))
                    .SendMessageAsync(message);
            }

            //Re-schedules the event
            ScheduleUpdate();
        }

        // Increments the day by one
        private void IncrementDay()
        {
            // Variable to hold the current last day
            var today = ScheduleGenerator.GetLetterDay();

            // Checks for end of cycle week
            if (today++ == 'H')
                today = 'A';

            // Updates the day in the database
            DatabaseManager.SetDay("" + today);
        }
    }
}