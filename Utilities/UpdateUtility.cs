﻿using Discord.WebSocket;

namespace squishybot_dotnet.Utilities
{
    public static class UpdateUtility
    {
        // Boolean to hold weather or not the command is active
        public static bool Activated;

        // Object to hold the current user
        public static SocketUser Executor;

        // Integer to remember the current step
        private static int _step = 1;

        // String to hold the date
        private static string _date = "";

        // Handles the multi-line update
        public static async void UpdateDialog(SocketMessage rawMessage)
        {
            // Gets the breakdown
            var content = rawMessage.Content;
            var channel = rawMessage.Channel;

            // Cleans up the message for the commands to process
            content = content.ToUpper().Replace(" ", "");

            // Checks for user cancellation
            if (content.Equals("QUIT"))
            {
                ResetData();
                await channel.SendMessageAsync("Canceling Request!");
                return;
            }

            // Runs dialog according to the current step
            if (_step == 1)
            {
                // Checks for the exact format
                if (content.Length == 10 && content.IndexOf("/") == 2 && content.LastIndexOf("/") == 5)
                {
                    // Adds the date and identifier to the changes
                    _date = content;

                    // Increments the current step
                    _step++;

                    // Sends the next-step message
                    await channel.SendMessageAsync(Executor.Mention +
                                                   " Enter the number of the day type change, the valid options are: \n" +
                                                   "1) `Cancel School` \n" +
                                                   "2) `Two Hour Delay` \n" +
                                                   "3) `Three Hour Delay` \n" +
                                                   "4) `Early Dismissal` \n" +
                                                   "5) `Emergency Early Dismissal` \n" +
                                                   "6) `PLC` \n" +
                                                   "7) `Regular` \n" +
                                                   "8) `None`");
                }
                else
                    await channel.SendMessageAsync("Invalid date entered. Format: `mm/dd/yyyy`, \n" +
                                                   "If the date has a single digit, the first zero is required");
            }
            else if (_step == 2)
            {
                //Checks if choice is valid
                if (content.Equals("1") || content.Equals("2") || content.Equals("3") || content.Equals("4") ||
                    content.Equals("5") || content.Equals("6") || content.Equals("7") || content.Equals("8"))
                {
                    //Switch statement for choice
                    switch (content)
                    {
                        case "1":
                            DatabaseManager.AddEvent(_date, "Cancel School");
                            break;
                        case "2":
                            DatabaseManager.AddEvent(_date, "Two Hour Delay");
                            break;
                        case "3":
                            DatabaseManager.AddEvent(_date, "Three Hour Delay");
                            break;
                        case "4":
                            DatabaseManager.AddEvent(_date, "Early Dismissal");
                            break;
                        case "5":
                            DatabaseManager.AddEvent(_date, "Emergency Early Dismissal");
                            break;
                        case "6":
                            DatabaseManager.AddEvent(_date, "PLC");
                            break;
                        case "7":
                            DatabaseManager.AddEvent(_date, "Regular");
                            break;
                        default:
                            await channel.SendMessageAsync("Canceling request!");
                            ResetData();
                            return;
                    }

                    // Sends the completed message
                    await channel.SendMessageAsync("Finished writing changes!");

                    // Resets the temp data
                    ResetData();
                }
                else
                    await channel.SendMessageAsync("Invalid choice, please try again!");
            }
        }

        // Resets temp data
        private static void ResetData()
        {
            Activated = false;
            Executor = null;
            _date = "";
            _step = 1;
        }
    }
}