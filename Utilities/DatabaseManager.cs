﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace squishybot_dotnet.Utilities
{
    public static class DatabaseManager
    {
        // Database Location
        private static readonly string DbLocation = "Data Source=" + Environment.CurrentDirectory + "SquishyData.db";

        // Returns results based on passed table and field
        public static List<string> ReadData(string table, string field)
        {
            // Variable to hold the query results
            var results = new List<string>();
            try
            {
                // Registers SQLite connection
                var dbConn = new SQLiteConnection(DbLocation);

                // Opens the connection
                dbConn.Open();

                // Creates the Command
                var sqLiteCommand = dbConn.CreateCommand();
                sqLiteCommand.CommandText = "SELECT " + field + " FROM " + table;

                // Reads query responce
                var sqLiteDataReader = sqLiteCommand.ExecuteReader();
                while (sqLiteDataReader.Read())
                    results.Add(sqLiteDataReader.GetString(0));

                // Closes the connection
                dbConn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return results;
        }

        // Generic database writing method
        private static void WriteData(string sql)
        {
            try
            {
                // Registers SQLite connection
                var dbConn = new SQLiteConnection(DbLocation);

                // Opens the connection
                dbConn.Open();

                // Creates the Command
                var sqLiteCommand = dbConn.CreateCommand();
                sqLiteCommand.CommandText = sql;
                sqLiteCommand.ExecuteNonQuery();

                // Closes the connection
                dbConn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        // Returns what the type of day it is on a given date
        public static string GetDateData(string date)
        {
            var type = "Regular";
            foreach (var result in ReadData("SchoolEvents WHERE Date IS \"" + date + "\"", "Event"))
                type = result;
            return type;
        }

        // Sets the letter day in the database
        public static void SetDay(string day)
        {
            WriteData($"UPDATE Today SET Letter= \"" + day + "\"");
        }

        //Adds a new event to the database
        public static void AddEvent(string date, string schoolEvent)
        {
            WriteData($"INSERT INTO SchoolEvents (Date, Event) " +
                      "VALUES(\"" + date + "\", \"" + schoolEvent + "\")");
        }
    }
}