﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using squishybot_dotnet.Services;
using squishybot_dotnet.Utilities;

namespace squishybot_dotnet
{
    class Program
    {
        // Asynchronous contex for the discord bot
        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        // Main async method
        public async Task MainAsync()
        {
            // Disposable service provider using Dependency Injection.
            using (var services = ConfigureServices())
            {
                var client = services.GetRequiredService<DiscordSocketClient>();

                // Logs the discord connection events
                client.Log += LogAsync;
                services.GetRequiredService<CommandService>().Log += LogAsync;

                // Discord bot config settings
                await client.LoginAsync(TokenType.Bot, Config.DiscordToken);
                await client.SetGameAsync("Tech Team | " + CommandHandlingService.Prefix + "help");
                await client.StartAsync();

                // Registers command handler.
                await services.GetRequiredService<CommandHandlingService>().InitializeAsync();

                // Blocks the program until it is closed.
                await Task.Delay(-1);
            }
        }

        // Logger method
        private Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());
            return Task.CompletedTask;
        }

        // Service configuration method
        private ServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton<DiscordSocketClient>()
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .BuildServiceProvider();
        }
    }
}